const axios = require('axios');

console.log(process.env.SLACK_WEBHOOK);

const {SLACK_WEBHOOK:sw, SLACK_CHANNEL:sc, SLACK_USERNAME:su} = process.env;

axios.post(
   sw,
  {
    channel: sc,
    username: su,
    blocks: [
      {
        type: 'divider',
      },
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text:
            "*Lucas Surowy*\n:star::star::star::star: 1638 reviews\n La Vitus a clairement changée ma vie. Ce doux brevage est vraiment un lifechanger dont on ne peut se passer. L'essayer c'est l'adopter !",
        },
        accessory: {
          type: 'image',
          image_url:
            'https://www.weihenstephaner.com/fileadmin/_processed_/8/e/csm_Vitus_9839e8cdb5.png',
          alt_text: 'alt text for image',
        },
      },
      {
        type: 'image',
        title: {
          type: 'plain_text',
          text: 'Je veux une Vitus',
          emoji: true,
        },
        image_url:
          'https://www.weihenstephaner.de/fileadmin/user_upload/Vitus_IV_Farbe_klein_.jpg',
        alt_text: 'marg',
      },
    ],
  },
);
